//=../../bower_components/jquery/dist/jquery.min.js

(function () {
    window.onload = function () {
        $('.overview__questions')
            .find('.question-avatar, .question-header__name')
            .each(function (index, element) {
                $(element).on('click', function () {
                    var id = parseInt($(element).attr('id'));

                    $('#question_popup_' + id)
                        .toggleClass('question-popup_show');
                    $(':not(#question_popup_' + id + ')')
                        .removeClass('question-popup_show');
                    $('#' + id + '_question_avatar')
                        .toggleClass('question-avatar_popup');
                    $(':not(#' + id + '_question_avatar)')
                        .removeClass('question-avatar_popup');
                    $('#' + id + '_question_name')
                        .toggleClass('question-header__name_popup');
                    $(':not(#' + id + '_question_name)')
                        .removeClass('question-header__name_popup');
                })
            });
    }
})();