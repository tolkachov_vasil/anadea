(function ($) {

    $.fn.parseInterval = function () {

        var utils = {
            toUTC: function (date) {
                if (!date || !date.getFullYear) return 0;
                return Date.UTC(date.getFullYear(),
                    date.getMonth(), date.getDate());
            },

            getDaysDiff: function daysInterval(date1, date2) {
                return (this.toUTC(date1) - this.toUTC(date2)) / 24 / 60 / 60 / 1000;
            }

        };

        var dates = this.attr('data-interval').split(',').sort();
        var len = dates.length;
        if (len < 2) {
            return this;
        }
        var date1 = new Date(dates[0]);
        var date2 = new Date(dates[len - 1]);
        var days = utils.getDaysDiff(date2, date1);

        var dateArr1 = date1.toString().split(' ');
        var dateArr2 = date2.toString().split(' ');

        var mount1 = dateArr1[1] + ' ';
        var mount2 = dateArr2[1] + ' ';
        var day1 = dateArr1[2];
        var day2 = dateArr2[2];
        var year1 = dateArr1[3] + ' ';
        var year2 = dateArr2[3] + ' ';

        if (mount1 === mount2 && days < 32) {
            mount2 = ''
        }

        if (year1 === year2) {
            year1 = '';
            year2 = '';
        }

        var outDiff = year1 + mount1 + day1
            + ' - ' + year2 + mount2 + day2;
        var outDays = ', ' + days + ' day'
            + (days > 1 ? 's' : '');
        var out = outDiff + outDays;
        this.text(out);

        return this;

    };
})(jQuery);