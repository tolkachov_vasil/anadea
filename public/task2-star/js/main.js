//=../../bower_components/lodash/dist/lodash.js
//=../../bower_components/jquery/dist/jquery.js

(function () {
    $(document).ready(function () {
        var BLOCK_WIDTH = 198;
        var DOUBLE_BLOCK_WIDTH = 408;
        var BLOCK_GAP = 10;
        var SOLID_GAP_TOLERANCE = 50;
        var rowsNum = 0;
        var xOffset = 0;
        var yOffset = 150;
        var carBlocks = [];
        var carDoubleBlocks = [];
        var gaps = [];
        var rowHeight = [];
        var blocks = [];
        var fixes = [];
        var solidGaps = [];

        var collectCarBlocks = function () {
            var blocks = [];
            $('.car').each(function (idx, el) {
                blocks.push(el);
            });
            carBlocks = _.sortBy(blocks, 'clientHeight').reverse();
        };

        var collectDoubleBlocks = function () {
            carDoubleBlocks = [];
            $('.car_double-width').each(function (idx, el) {
                carDoubleBlocks.push(el);
            });
            carDoubleBlocks = _.sortBy(carDoubleBlocks, 'clientHeight').reverse();
        };

        var setPosition = function (el, x, y) {
            $(el).offset({ left: x, top: y });
        };

        var getShortestRow = function () {
            var min = Infinity;
            var shortestRow = 0;
            var last = 0;
            for (var i = 0; i < rowsNum; i++) {
                last = rowHeight[i];
                if (last < min) {
                    min = last;
                    shortestRow = i;
                }
            }
            return shortestRow;
        };

        var hideCarBlocks = function () {
            $('.car').each(function (idx, car) {
                $(car).offset({ left: '-200%' });
            });
        };

        var resetAll = function () {
            rowHeight = [];
            blocks = [];
            for (var i = 0; i < rowsNum; i++) {
                rowHeight[i] = 0;
            }
            blocks = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []];
            $('.car__text').css({ paddingBottom: 0 + 'px' });
        };

        var updateSize = function () {
            var width = $(window).width();
            yOffset = 150 + (width < 480 ? 150 : 0);
            rowsNum = Math.floor(width / (BLOCK_WIDTH + BLOCK_GAP));
            xOffset = (width % (BLOCK_WIDTH + BLOCK_GAP)) / 2;
            if (rowsNum === 0) {
                rowsNum = 1;
                xOffset = 0;
            }
        };

        var placeBlocks = function (insertFirst) {
            var row = 0;
            var double = false;
            var lGap = false;
            var rGap = false;

            if (insertFirst > 0) {
                for (var i = 0; i < insertFirst; i++) {
                    row = getShortestRow();
                    if (row === rowsNum - 1) row = 0;
                    insertDoubleBlock(row);
                }
            }

            $(carBlocks).each(function (idx, car) {
                if (!$(car).hasClass('car_double-width')) {
                    row = getShortestRow();
                    blocks[row].push(car);
                    x = row * (BLOCK_WIDTH + BLOCK_GAP) + xOffset;
                    y = rowHeight[row] + yOffset;
                    setPosition(car, x, y);
                    rowHeight[row] += car.clientHeight + BLOCK_GAP;
                    lGap = false;
                    rGap = false;
                    if (row > 0) lGap = checkSolidGap(row, row - 1);
                    if (lGap) {
                        insertDoubleBlock(row - 1);
                    } else {
                        if (row < rowsNum - 1) rGap = checkSolidGap(row, row + 1);
                        if (rGap) {
                            insertDoubleBlock(row);
                        }
                    }

                }
            });

            var dbl = carDoubleBlocks.length;
            if (dbl > 0) {
                resetAll();
                collectDoubleBlocks();
                placeBlocks(dbl);
            }
        };

        var checkSolidGap = function (row1, row2) {
            var rowToFix = 0;
            var h = 0;
            var h1 = rowHeight[row1];
            var h2 = rowHeight[row2];
            var delta = Math.abs(h1 - h2);
            if (delta <= SOLID_GAP_TOLERANCE) {
                h = Math.max(h1, h2);
                if (delta !== 0) {
                    rowToShift = (h1 > h2 ? row2 : row1);
                    fixBlockHeight(rowToShift, delta);
                }
                return true;
            }
            return false;
        };

        var getLastBlock = function (row) {
            var bRow = blocks[row];
            var block = bRow[bRow.length - 1];
            return block;
        };

        var fixBlockHeight = function (row, delta) {
            var block = getLastBlock(row);

            if (block.fake) {
                block.clientHeight += delta;
                var b2 = getLastBlock(row - 1);
                $(b2).children('.car__text').css({ paddingBottom: delta + 'px' });
                rowHeight[row - 1] += delta;
            } else {
                $(block).children('.car__text').css({ paddingBottom: delta + 'px' });
                if (block.clientWidth > BLOCK_WIDTH) {
                    var b1 = getLastBlock(row + 1);
                    $(b1).children('.car__text').css({ paddingBottom: delta + 'px' });
                    rowHeight[row + 1] += delta;
                }
            }
            rowHeight[row] += delta;
        };

        var insertDoubleBlock = function (row) {
            var block = carDoubleBlocks.shift();
            if (!block) return;
            var h = block.clientHeight + BLOCK_GAP;
            var x = row * (BLOCK_WIDTH + BLOCK_GAP) + xOffset;
            setPosition(block, x, rowHeight[row] + yOffset);
            rowHeight[row] += h;
            rowHeight[row + 1] += h;
            blocks[row].push(block);
            blocks[row + 1].push({ fake: true, clientHeight: block.clientHeight });
        };

        var refresh = function () {
            collectDoubleBlocks();
            hideCarBlocks();
            updateSize();
            resetAll();
            placeBlocks(0);
        };

        $(window).resize(function () {
            refresh();
        });

        collectCarBlocks();
        refresh();

    });
})();