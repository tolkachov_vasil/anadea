//=../../bower_components/jquery/dist/jquery.min.js

(function ($) {

    $.fn.popup = function (delay) {
        var GAP = 20;
        if (delay === undefined) { delay = 500; }
        var popups = [];
        var idPrefix = 'popup_';
        var lastId = 0;

        var utils = {

            createAnimation: function () {
                $('head').append(
                    '<style> .popup_show {'
                    + 'animation: fadein ' + (delay + 50) + 'ms;'
                    + 'opacity: .7;}'
                    + '</style>');
            },

            scanPopups: function () {
                popups = $('[data-popup-text]');
            },

            getArgs: function (el) {
                var args = {
                    position: $(el).data('popup-position'),
                    offset: $(el).data('popup-offset'),
                    text: $(el).data('popup-text')
                };
                return args;
            },

            insertPopup: function (el, args) {
                var id = idPrefix + lastId++;
                $(el).after(
                    '<div class="popup '
                    + args.position + ' '
                    + (args.offset ? 'offset_' + args.offset : '')
                    + '"' + 'id="' + id + '"' + '>'
                    + args.text
                    + '</div>');

                return id;
            },

            placePopup: function (el, id) {
                var xe = $(el).offset().left;
                var ye = $(el).offset().top;
                var he = $(el).height();
                var we = $(el).width();

                var popup = $('#' + id);
                var xp = xe;
                var yp = ye + 2;
                var hp = $(popup).height();
                var wp = $(popup).width();
                switch (args.position) {
                    case 'top':
                        xp = xe + we / 2 - wp / 2;
                        yp = ye - he / 2 - hp - GAP / 2;
                        break;
                    case 'bottom':
                        xp = xe + we / 2 - wp / 2;
                        yp = ye + he / 2 + hp + GAP / 2;
                        break;
                    case 'left':
                        xp = xe - we / 2 - wp / 2 - GAP;
                        break;
                    case 'right':
                        xp = xe + we + GAP;
                        break;
                }
                switch (args.offset) {
                    case 'left':
                        xp -= wp * .4;
                        break;
                    case 'right':
                        xp += wp * .4;
                        break;
                }
                $(popup).offset({ left: xp, top: yp });
                $(el).on('mouseover', function () {
                    $('#' + id).addClass('popup_show');
                });
                $(el).on('mouseout', function () {
                    $('#' + id).removeClass('popup_show');
                });
            }


        };

        utils.scanPopups();
        utils.createAnimation();
        var args, id;
        popups.each(function (idx, popup) {
            args = utils.getArgs(popup);
            id = utils.insertPopup(popup, args);
            utils.placePopup(popup, id);
        });

    };
})(jQuery);